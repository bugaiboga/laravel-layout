<?php

namespace App\Models;

use Illuminate\Support\Facades\Lang;

class Product extends BaseModel
{
    protected $with = ['photos', 'category'];

    protected $casts = [
        'params' => 'collection',
    ];

    protected $guarded = ['id'];

    public function category()
    {
        return $this->hasOne('App\Models\Categories', 'id', 'category_id');
    }
}
