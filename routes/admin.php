<?php

/*
 *
 *  admin panel
 *
 */


Route::get('test', function(){
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\ImportProducts());
});


Route::get('admin/login', 'Admin\AdminController@getLogin');

Route::get('logout', 'Auth\AuthController@logout');

Route::post('admin/login', 'Admin\AdminController@postLogin');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/', ['uses' => 'Admin\AdminController@index', 'as' => 'admin.index']);

    Route::get('json/changevisibility', 'Admin\JsonController@getChangeVisibility');

    Route::get('json/delete', 'Admin\JsonController@getDeleteModel');

    Route::get('json/delete-category-icon', 'Admin\JsonController@deleteCategoryIcon');

    Route::resource('lists', 'Admin\ListsController');

    Route::get('get-users', 'Admin\UserController@ajaxData');

    Route::resource('users', 'Admin\UserController');

    Route::resource('content', 'Admin\ContentController');

    Route::resource('categories', 'Admin\CategoriesController');

    Route::get('get-categories', 'Admin\CategoriesController@ajaxData');

    Route::resource('products', 'Admin\ProductsController');

    Route::get('get-products', 'Admin\ProductsController@ajaxData');

    Route::get('clear-cache', 'Admin\AdminController@clearCache');

    Route::get('translations', 'Admin\TranslationsController@edit');

    Route::post('translations', 'Admin\TranslationsController@save');

    Route::resource('parameters',       'Admin\ParametersController');

    Route::get('json/remove-parameter', 'Admin\ParametersController@removeParameter');

    Route::get('json/remove-parameter-value', 'Admin\ParametersController@removeParameterValue');

    Route::get('json/get-category-parameters',  'Admin\ParametersController@getCategoryParameters');

    Route::resource('orders', 'Admin\OrdersController');

    Route::get('get-orders', 'Admin\OrdersController@ajaxData');
});
