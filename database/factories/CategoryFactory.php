<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Categories::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description_short' => $faker->text(50),
        'description' => $faker->text(200),
        'slug' => $faker->slug(3),
    ];
});
