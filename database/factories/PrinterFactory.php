<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Printer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'printer_category_id' => function () {
            $c = factory(App\Models\PrinterCategory::class)->create();
            return $c->id;
        },
        'printer_type_id' => function () {
            $c = factory(App\Models\PrinterType::class)->create();
            return $c->id;
        },
    ];
});
