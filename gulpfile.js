const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const bs = require("browser-sync").create();
const sourceMaps = require("gulp-sourcemaps");
const concatincation = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;

let autoprefixBrowsers = [
    "> 1%",
    "last 2 versions",
    "firefox >= 4",
    "safari 7",
    "safari 8",
    "IE 8",
    "IE 9",
    "IE 10",
    "IE 11"
];

function style() {
    return (
        gulp
            .src("./scss/style.scss")
            .pipe(sass())
            /*     .pipe(autoprefixer(
               { browsers: autoprefixBrowsers }
           ))*/
            .pipe(gulp.dest("./css"))
            .pipe(bs.stream())
    );
}

function concat() {
    return gulp
        .src(["./node_modules/uikit/dist/js/uikit.js", "./js/script.js"])
        .pipe(concatincation("common.js"))
        .pipe(uglify())
        .pipe(gulp.dest("./js"));
}

function watch() {
    bs.init({
        proxy: "http://herastrauboutique.local",
        port: 3000
    });

    gulp.watch("./scss/**/*.scss", style);
    gulp.watch("./*.php").on("change", bs.reload);
}

exports.style = style;
exports.concat = concat;
exports.watch = watch;
