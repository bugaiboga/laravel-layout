<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'category_id' => function () {
            $c = factory(App\Models\Categories::class)->create();
            return $c->id;
        },
        'product_number' => $faker->name,
        'name' => $faker->name,
        'name_ro' => $faker->name,
        'name_en' => $faker->name,
        'description' => $faker->text(),
        'description_ro' => $faker->text(),
        'description_en' => $faker->text(),
        'characteristics' => $faker->text(),
        'characteristics_ro' => $faker->text(),
        'characteristics_en' => $faker->text(),
        'price' => $faker->randomFloat(),
        'price_today' => $faker->randomFloat(),
        'sale' => $faker->randomFloat(),
        'sale_vip' => $faker->randomFloat(),
        'sale_gold' => $faker->randomFloat(),
        'sale_silver' => $faker->randomFloat(),
        'slug' => $faker->slug(3),
        'type' => $faker->randomElement([0, 1, 2]),
        'top_mainpage' => $faker->randomElement([true, false]),
        'top_category' => $faker->randomElement([true, false]),
        'recommended' => $faker->randomElement([true, false]),
        'bestseller' => $faker->randomElement([true, false]),
        'just_purchased' => $faker->randomElement([true, false]),
        'navigation_menu' => $faker->randomElement([true, false]),
        'availability' => $faker->randomElement([0, 1, 2]),
        'is_virtual' => $faker->randomElement([true, false]),
    ];
});
