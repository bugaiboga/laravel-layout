<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartridgePrinterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cartridge_printer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cartridge_id')->unsigned();
            $table->foreign('cartridge_id')->references('id')->on('cartridges')->onDelete('cascade');

            $table->integer('printer_id')->unsigned();
            $table->foreign('printer_id')->references('id')->on('printers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cartridge_printer');
    }
}
