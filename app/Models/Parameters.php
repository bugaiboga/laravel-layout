<?php

namespace App\Models;


use Illuminate\Support\Facades\Lang;

class Parameters extends BaseModel
{
    public $timestamps = false;

    protected $with = ['values'];

    protected $casts = [
        'params' => 'collection',
    ];

    const TYPE_INPUT = 0;
    const TYPE_SELECT = 1;
    const TYPE_STRING = 2;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'parameters_products', 'parameter_id', 'product_id');
    }

    public function values()
    {
        return $this->hasMany('App\Models\ParametersValues', 'parameter_id');
    }

    public function valuesWithCount($category_id, $productsIds = [])
    {
        $query = $this->values()->withCount(['products' => function ($query) use ($category_id, $productsIds) {
            $query->where('category_id', $category_id);
            if (count($productsIds) > 0) {
                $query->whereIn('products.id', $productsIds);
            }
        }])->orderBy('products_count', 'desc');

        if (count($productsIds) > 0) {
            $query->whereHas('products', function($query) use ($productsIds, $category_id) {
                $query->where('category_id', $category_id);
            });
        }


        return $query;
    }

    public function getSelectedValue()
    {
        $locale = Lang::locale();
        if ($locale == config('app.base_locale')) {
            $field = 'value';
        } else {
            $field = 'value_' . $locale;
        }

        if ($this->type == Parameters::TYPE_INPUT) {
            return $this->pivot->value;
        }

        if ($this->type == Parameters::TYPE_SELECT) {
            $values = $this->values->pluck($field, 'id')->toArray();
            if (isset($values[$this->pivot->value_id])) {
                return $values[$this->pivot->value_id];
            }
        }

        if ($this->type == Parameters::TYPE_STRING) {
            return $this->pivot->$field;
        }

        return '';
    }
}
