<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lists;
use App\Models\Photos;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ListsController extends Controller
{
    public function index(Request $request)
    {
        $id      = $request->id;

        if ($id > 0) {
            $parent  = Lists::where('id', $id)->with('children')->first();
            $data    = $parent->children;
        } else {
            $data    = Lists::where('parent_id', 0)->get();
        }

        $parents = $this->getParentsArray();
        $model = 'lists';
        return view('admin.lists.index')->with(compact('data', 'parents', 'id', 'model', 'parent'));
    }

    public function create(Request $request)
    {
        //$parents    = $this->getParentsArray();
        $parent_id         = $request->id;
        $parents    = Lists::all()->pluck('name', 'id')->toArray();

        $view = null;

        switch ($parent_id) {
            case 8:
                $view = view('admin.lists.slider');
                break;
            default:
                $view = view('admin.lists.edit');
                break;
        }

        return $view->with(compact('parents', 'parent_id'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request);
    }

    private function save(Request $request, $id = null)
    {
        // store
        if (!isset($id)) {
            $data = new Lists();
        } else {
            $data = Lists::find($id);
        }

        $data->name              = $request->name;
        $data->created_at        = Carbon::now()->toDateString();
        $data->parent_id         = $request->parent_id;

        //params
        $params = [];
        if (isset($request->params)) {
            foreach ($request->params as $key => $value) {
                $params[] = ['key' => $key, 'value' => $value];
            }
            $data->params = serialize($params);
        }

        if (isset($request->description)) {
            $data->description = $request->description;
        }
        $data->description_short = $request->description_short;
        $data->meta_description  = $request->meta_description;
        $data->meta_keywords     = $request->meta_keywords;
        $data->title             = $request->title;

        $data->save();

        $this->saveVideo($request, $data->id);
        $this->UpdatePhotos($request, $data->id);

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect()->route('admin.lists.edit', $data->id);
    }

    public function saveVideo(Request $request, $id)
    {
        ini_set('upload_max_filesize', '128MB');

        if ($request->hasFile('video')) {
            $video = $request->file('video');
            $path = public_path('uploaded/video/') . $video->getClientOriginalName();

            //сохраняемс в таблицу
            Photos::updateOrCreate(
                ['table_id' => $id, 'table' => 'lists_video'],
                ['source' => $video->getClientOriginalName()]
            );

            //перемещаем
            File::move($video->getRealPath(), $path);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data       = Lists::find($id);
        //$parents    = $this->getParentsArray();
        $parents    = Lists::all()->pluck('name', 'id')->toArray();
        $parent_id  = $data->parent_id;

        $view = null;
        switch ($id) {
            case 1:
                $view = view('admin.lists.background');
                break;
            case 2:
                $view = view('admin.lists.work');
                break;
            case 3:
                $view = view('admin.lists.video_block');
                break;
            case 4:
                $view = view('admin.lists.apartments');
                break;
            default:
                $view = view('admin.lists.edit');
                break;
        }

        /*switch ($parent_id){
            case 3:
                $view = view('admin.lists.service');
                break;
            default:
                $view = view('admin.lists.edit');
                break;
        }*/

        return $view->with(compact('data', 'parents', 'parent_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Lists::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function getParentsArray()
    {
        $parent = Lists::where('parent_id', 0)->get();
        if ($parent->count() == 0) {
            return [];
        }
        return $parent->pluck('name', 'id')->toArray();
    }
}
