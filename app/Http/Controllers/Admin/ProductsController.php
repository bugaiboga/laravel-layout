<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = Product::query();
        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.products.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name.ru'  => 'required',
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        $fields = $request->except([
            'file_upload', 'photos', 'parameters',
            'value_ru', 'value_ro', 'value_en', 'value_id'
        ]);

        // store
        if (!isset($id)) {
            $data = Product::create();
        } else {
            $data = Product::find($id);
        }

        $data->update($fields);

        // checkboxes
        $checkboxes = [
            'enabled', 'top_mainpage', 'top_category',
            'recommended', 'bestseller', 'just_purchased',
            'navigation_menu', 'has_gift', 'is_virtual',
            'new', 'top_sale',
        ];

        foreach ($checkboxes as $checkbox) {
            $data->setAttribute($checkbox, $request->has($checkbox) ? true : false);
        }

        $data->save();

        $this->UpdatePhotos($request, $data->id);

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect(route('admin.products.edit',  $data->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	    $data = Product::find($id);

        return view('admin.products.edit')->with(compact('data'))->with($this->needs());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name.ru' => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    private function needs()
    {
        $categories = Categories::pluck('name','id')->toArray();
        return compact('categories');
    }
}
