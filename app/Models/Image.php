<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $timestamps = false;

    public function getSmallPhoto()
    {
        clearstatcache();

        $thumb = config('photos1c.thumbs_url') . $this->attributes['filename'];

        if(file_exists(public_path() . $thumb)){
            return config('photos1c.thumbs_url') . $this->attributes['filename'];
        }

        return config('photos1c.images_url') . $this->attributes['filename'];
    }

    public function getBigPhoto()
    {
        return config('photos1c.images_url') . $this->attributes['filename'];
    }
}
