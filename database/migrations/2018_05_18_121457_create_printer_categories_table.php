<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrinterCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('printer_categories', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name');
            $t->string('name_ro');
            $t->string('name_en');
            $t->text('description');
            $t->text('description_ro');
            $t->text('description_en');
            $t->boolean('enabled')->default(true);
            $t->integer('sort');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('printer_categories');
    }
}
