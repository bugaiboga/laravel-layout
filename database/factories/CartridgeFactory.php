<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Cartridge::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(),
        'mfg' => $faker->text(20),
        'part_number' => $faker->text(20),
        'price' => $faker->randomFloat(),
        'shipping' => $faker->address,
    ];
});
