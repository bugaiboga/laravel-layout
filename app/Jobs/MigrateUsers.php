<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class MigrateUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = DB::connection('superoffice_old')
            ->table('users')
            //->limit(10)
            ->get();

        $counter = 0;
        foreach ($data as $item) {
            $params = [
                'company' => $item->company,
                'city' => $item->city,
                'fisc' => $item->fisc,
                'bank' => $item->bank,
                'bank_account' => $item->count,
                'tva' => $item->nds,
                'fax' => $item->fax,
                'login' => $item->login,
                'is_manager' => $item->is_manager,
                'newpassword' => $item->newpassword,
            ];

            $values = [
                'name' => $item->fio,
                'email' => $item->email,
                'open_password' => $item->password,
                'address' => $item->address,
                'phone' => $item->phone,
            ];

            // обновляем либо создаем пользователя
            $user = User::updateOrCreate(['email' => $item->email], $values);

            $user->params = $params;
            $user->save();

            echo ++$counter . '. ' . $user->email . PHP_EOL;
        }
    }
}
