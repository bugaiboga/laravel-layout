<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    public function index()
    {
        $user = User::find(Auth::id());

        if (!$user) {
            return redirect()->route('login');
        }

        $ordersPage = true;

        $orders = Orders::where('user_id', $user->id)->get();

        return view('orders')->with(compact('ordersPage', 'orders'));
    }

    public function orderInfo(Request $request)
    {
        $order_id = $request->order_id;

        $order = Orders::where('id',$order_id)
                    ->where('user_id', auth()->id())
                    ->firstOrFail();

        $order_cart = $order->getCartItems();

        $order_products = array();

        foreach ($order_cart as $k => $oc){
            $product = Product::find($oc["product_id"]);

            $order_products[$k] = $oc;
            $order_products[$k]["image"] = $product->getSmallPhoto();
            $order_products[$k]["total_amount"] = $order->getSubtotalAmount();
        }

        $ordersInfoPage = true;

        return view('order-info')->with(compact('ordersInfoPage', 'order', 'order_products'));
    }
}