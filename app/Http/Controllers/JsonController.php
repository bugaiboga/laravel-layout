<?php

namespace App\Http\Controllers;

use App\Models\TecDoc;
use Maatwebsite\Excel\Classes\Cache;

class JsonController extends Controller
{
    private function json_response($data = null, $errors = null)
    {
        if (!is_null($errors) && count($errors) > 0) {
            return response()->json(['success' => 'false', 'data' => $errors]);
        }
        return response()->json(['success' => 'true', 'data' => $data]);
    }

    public function getModelsByManufacturerId($id)
    {
        //saving to cache manufacturer ID
        cache(['sel_manufacturer_id' => $id], config('cache.arrays_cache_minutes'));

        $data = TecDoc::getModelsByManufacturerId($id, config('tecdoc.lang'));
        return $this->json_response($data);
    }

    public function getTypesByModelId($id)
    {
        //saving to cache manufacturer ID
        cache(['sel_model_id' => $id], config('cache.arrays_cache_minutes'));

        $data = TecDoc::getTypesByModelId($id, config('tecdoc.lang'));
        return $this->json_response($data);
    }
}
