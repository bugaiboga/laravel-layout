<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\Products;
use App\Models\TecDoc;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class MailController extends Controller
{
    private $from_mail;
    private $from_name;
    private $to_mail;
    private $to_name;

    public function __construct(){
        $this->from_mail = env('MAIL_USERNAME');
        $this->from_name = trans('common.mail_cart_description');
        $this->to_mail   = config('custom.admin_email');
        $this->to_name   = env('APP_URL');
    }


    public function sendCard(Request $request)
    {
        $data = [];
        $all_amount = 0;

        $json_data = json_decode($request->cart);

        foreach ($json_data->items as $item) {
            $id = $item->_id;
            $quantity = $item->_quantity;
            $product = TecDoc::getPartById($id, config('tecdoc.lang'));
            $amount = $quantity * $product->getPrice();
            $data[] = [
                'id'        => $product->getId(),
                'name'      => $product->getName(),
                'quantity'  => $quantity,
                'price'     => $product->getPrice(),
                'article'   => $product->getProductNumber(),
                'link'      => route('get-part', [str_slug($product->getBrandName()), $product->id]),
                'amount'    => $amount
            ];

            $all_amount += $amount;
        }

        $params = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'text' => $request->text,
            'payment' => $request->payment,
            'delivery' => $request->delivery,
            'data' => $data,
            'all_amount' => $all_amount
        ];

        //adding order
        $order = new Orders();
        $order->user_id = (!is_null(Auth::id()) ? Auth::id() : 0);
        $order->data = serialize($params);
        $order->save();

        //saving order ID
        session(['order_id' => $order->id]);

        //sending email
        Mail::send('emails.send-card', $params, function ($m) {
            $m->from($this->from_mail, $this->from_name)
                ->to($this->to_mail, $this->to_name)
                ->subject(trans('common.mail_cart_header'));
        });
    }

    public function makeContact(Request $request){
        Mail::send('emails.make-contact', ['name' => $request->name, 'email' => $request->email, 'text'=>$request->text], function ($m) {
            $m->from($this->from_mail, $this->from_name)
                ->to($this->to_mail,   $this->to_name)
                ->subject('Сообщение обратной связи');
        });
    }

    public function callbackPrice(Request $request){
        $product = TecDoc::getPartById($request->product_id, config('tecdoc.lang'));
        Mail::send('emails.callback-price', ['phone' => $request->phone, 'product' => $product], function ($m) {
            $m->from($this->from_mail, $this->from_name)
                ->to($this->to_mail,   $this->to_name)
                ->subject('Сообщение "Узнать цену"');
        });
    }

    public function sendEmail(Request $request){
        Mail::send('emails.send-email', ['email' => $request->email ], function ($m) {
            $m->from($this->from_mail, $this->from_name)
              ->to($this->to_mail,   $this->to_name)
              ->subject('Подписка на рассылку');
        });
    }   

    public function callbackPhone(Request $request){
        Mail::send('emails.callback-phone', ['name' => $request->name, 'phone' => $request->phone], function ($m) {
            $m->from($this->from_mail, $this->from_name)
                ->to($this->to_mail,   $this->to_name)
                ->subject('Заказ звонка');
        });
    }
}
