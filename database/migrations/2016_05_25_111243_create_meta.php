<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta', function(Blueprint $table) {
            $table->increments('id');
            $table->string('meta_description');
            $table->string('meta_description_ro');
            $table->string('meta_description_en');
            $table->string('meta_keywords');
            $table->string('meta_keywords_ro');
            $table->string('meta_keywords_en');
            $table->string('title');
            $table->string('title_ro');
            $table->string('title_en');
            $table->integer('table_id')->index();
            $table->string('table', 20)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meta');
    }
}
