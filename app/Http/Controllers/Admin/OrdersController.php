<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    public function index()
    {
        return view('admin.orders.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = Orders::latest();
        return self::datatablesCommon($query)
            ->editColumn('name', function ($item) {
                $out = "<a href=" . route('admin.orders.edit', $item->id) . ">";
                $out .= "Заказ #" . $item->id . " (" . ($item->params['email'] ?? '') . ")";
                $out .= "</a>";
                return $out;
            })
            ->editColumn('company', function ($item) {
                return ($item->user->params['company'] ?? '');
            })
            ->make(true);
    }

    private function save(Request $request, $id){
        // store
        $order = Orders::find($id);
        $order->status = $request->status;
        $order->description = $request->description;
        $order->save();

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect()->route('admin.orders.edit', $order->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $order = Orders::find($id);
        return view('admin.orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Orders::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

}
