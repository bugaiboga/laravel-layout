<?php

namespace App\Http\Middleware;

use App\Models\Lists;
use App\Models\Categories;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class SharedVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $brands = Categories::enabled()->get();

        view()->share('brands', $brands);

        return $next($request);
    }
}
