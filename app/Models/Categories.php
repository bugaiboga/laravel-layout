<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends BaseModel
{
    protected $dates = ['created_at', 'updated_at'];
    protected $with = ['parents', 'photos'];

    public function children()
    {
        return $this->belongsToMany('App\Models\Categories', 'categories_xref', 'parent_id', 'child_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id')->whereIn('products.availability', [Product::AVAILABILITY_FULL, Product::AVAILABILITY_LOW]);
    }

    public function parents()
    {
        return $this->belongsToMany('App\Models\Categories', 'categories_xref',  'child_id', 'parent_id');
    }

    public function getFullPathAttribute()
    {
        return (isset($this->parents[0]) ? $this->parents[0]->name . ' > ' : '')  . $this->name;
    }

    public function parameters()
    {
        return $this->belongsToMany('App\Models\Parameters', 'parameters_categories', 'category_id', 'parameter_id')->orderBy('sort');
    }

    public function parametersWithCount($category_id, $productsIds = [])
    {
        $query = $this->parameters()->withCount(['products' => function ($query) use ($category_id, $productsIds) {
            $query->where('category_id', $category_id);
            if (count($productsIds) > 0) {
                $query->whereIn('products.id', $productsIds);
            }
        }]);

        if (count($productsIds) > 0) {
            $query->whereHas('products', function ($query) use ($productsIds, $category_id) {
                $query->where('category_id', $category_id);
                $query->whereIn('products.id', $productsIds);
            });
        }

        return $query;
    }

    public function icon()
    {
        return $this->hasOne('App\Models\Photos', 'table_id')->where('table', 'categories_icon');
    }
}
