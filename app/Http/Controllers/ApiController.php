<?php

namespace App\Http\Controllers;

use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Validator;

class ApiController extends Controller
{
    public function getCardInfo(Request $request)
    {
		header("Access-Control-Allow-Origin: *");
		
        $rules = [
            'card' => 'required|exists:cards,card_number',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'success' => false,
                'data'    => $validator->messages()
            ];
        }

        $card = Card::where('card_number', $request->card)->first();

        $response = [
            'success' => true,
            'data' => trans('common.valid_thru') . Carbon::createFromFormat('Y-m-d', $card->valid_thru)->format('d-m-Y'),
        ];


        return response()->json($response);
    }

}
