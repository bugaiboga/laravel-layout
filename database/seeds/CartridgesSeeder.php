<?php

use Illuminate\Database\Seeder;

class CartridgesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Cartridge::class, 100)->create();
    }
}
