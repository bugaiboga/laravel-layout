<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Parameters;
use App\Models\ParametersProducts;
use App\Models\Photos;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = Categories::query();
        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.categories.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:categories'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Categories();
        } else {
            $data = Categories::find($id);
        }

        $icon = $request->file('icon');

        if($icon) {
            $original_name = $icon->getClientOriginalName();

            if($data->icon()->count()){
                $photos = Photos::find($data->icon->id);
            }else{
                $photos = Photos::create();
            }
            $photos->source = $original_name;
            $photos->table_id = $id;
            $photos->table = 'categories_icon';
            $photos->save();

            File::move($icon->getPathname(), public_path('uploaded/').$original_name);
        }

        $data->name              = $request->name;
        $data->slug              = $request->slug;
        $data->description       = $request->description;
        $data->created_at        = Carbon::now()->toDateString();
        $data->title             = $request->title;
        $data->meta_keywords     = $request->meta_keywords;
        $data->meta_description  = $request->meta_description;
        $data->title             = $request->title;
        $data->top               = $request->top;
        $data->sort              = $request->sort;
        $data->reserve           = $request->color;
        $data->save();

        $this->UpdatePhotos($request, $data->id);

        //categories
        if ($request->parent) {
            $data->parents()->sync($request->parent, true);
        } else {
            $data->parents()->detach();
        }

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/categories');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	    $data    = Categories::find($id);
        $parents = $data->parents->pluck('id')->toArray();
        $category_parameters = $data->parameters()->orderBy('sort')->get();
        return view('admin.categories.edit')->with(compact('data', 'parents', 'category_parameters'))->with($this->needs());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:categories,id,{$id}'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Categories::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function needs()
    {
        $categories = Categories::pluck('name','id')->toArray();
        $parameters = Parameters::orderBy('name')->pluck('name','id')->toArray();
        return compact('categories', 'parameters');
    }
}
