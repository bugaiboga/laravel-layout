<?php

namespace App\Jobs;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class ImportQtyAndPrices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $filePath;
    private $textFileName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->filePath = storage_path('app/public/xml/goods_info.xml');
        $this->textFileName = 'goods_info.txt';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $xml = simplexml_load_file($this->filePath);

        $generatedDate = (string) $xml['Generated'];
        if (Storage::disk('public')->exists('xml/' . $this->textFileName)) {
            $fileContent = Storage::disk('public')->get('xml/' . $this->textFileName);
            if ($fileContent == $generatedDate) {
                echo 'Файл уже был загружен!' . PHP_EOL;
                return;
            }
        } else {
            Storage::disk('public')->put('xml/' . $this->textFileName, '');
        }

        $data = [];
        foreach ($xml->children() as $product) {
            preg_match('/^\d+(?:[.,]\d{1,2})?$/m', $product->product_price['product_price'], $matches);
            $data[] = [
                'product_code' => preg_replace( '/[^0-9]/', '', $product->product_code['product_code']),
                'product_number' => (string) $product->product_vCode['product_vCode'],
                'price' => (float) (isset($matches[0]) ? str_replace(",", ".", $matches[0]) : 0),
                'availability' => ImportProducts::getAvailability((string) $product->product_quantity['product_quantity']),
            ];
        }

        $counter = 0;
        foreach ($data as $item) {
            // обновляем продукт
            $product = Product::where('product_code', $item['product_code'])->first();
            if (isset($product)) {
                $product->update($item);
                echo ++$counter . '. ' . $product->name . PHP_EOL;
            }
        }

        Storage::disk('public')->put('xml/' . $this->textFileName, $generatedDate);
    }
}
