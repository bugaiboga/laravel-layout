<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class BaseModel extends Model
{
    /**
     * Create the slug from the title
     */
    public function setSlugAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['slug'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['slug'] = $value;
                    continue;
                }
                $this->attributes['slug_' . $key] = $value;
            }
        }
    }

    public function setMetaDescriptionAttribute($values)
    {
        $this->saveMeta($values, "meta_description");
    }

    public function setMetaKeywordsAttribute($values)
    {
        $this->saveMeta($values, "meta_keywords");
    }

    public function setTitleAttribute($values)
    {
        $this->saveMeta($values, "title");
    }



    public function setNameAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['name'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['name'] = $value;
                    continue;
                }
                $this->attributes['name_' . $key] = $value;
            }
        }
    }

    public function setDescriptionAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['description'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['description'] = $value;
                    continue;
                }
                $this->attributes['description_' . $key] = $value;
            }
        }
    }

    public function setDescriptionShortAttribute($values)
    {

        if (!is_array($values)) {
            $this->attributes['description'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['description_short'] = $value;
                    continue;
                }
                $this->attributes['description_short_' . $key] = $value;
            }
        }
    }

    public function getDescriptionAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['description_' . $locale])) {
            return $this->attributes['description_' . $locale];
        }
        return $this->attributes['description'];
    }

    public function getDescriptionShortAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['description_short_' . $locale])) {
            return $this->attributes['description_short_' . $locale];
        }

        return $this->attributes['description_short'];
    }

    public function getNameAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['name_' . $locale])) {
            return $this->attributes['name_' . $locale];
        }

        return  $this->attributes['name'];
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimeString($value)->format('d/m/Y');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photos', 'table_id')->where('table', $this->getTable())->orderBy('sort');
    }

    public function photos_work_plan()
    {
        return $this->hasMany('App\Models\Photos', 'table_id')->where('table', 'lists_photowork');
    }

    public function video()
    {
        return $this->hasOne('App\Models\Photos', 'table_id')->where('table', 'lists_video');
    }

    public function meta()
    {
        return $this->hasOne('App\Models\Meta', 'table_id')->where('table', $this->getTable());
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Reviews', 'table_id')->where('table', $this->getTable())->orderBy('created_at', 'desc');
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    public function scopeDisabled($query)
    {
        return $query->where('enabled', 0);
    }

    public function scopeTop($query)
    {
        return $query->where('top', 1);
    }

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name",    "LIKE", "%$keyword%")
                    ->orWhere("name_ro", "LIKE", "%$keyword%")
                    ->orWhere("name_en", "LIKE", "%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%")
                    ->orWhere("description_ro", "LIKE", "%$keyword%")
                    ->orWhere("description_en", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    private function saveMeta($values, $type)
    {
        $table    = $this->getTable();
        //save model before saving meta
        if (!isset($this->id)) $this->save();
        $table_id = $this->id;

        $meta = Meta::where('table', $table)->where('table_id', $table_id)->first();
        if (!isset($meta)) {
            $meta = new Meta();
        }
        if (!empty($values)) {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $meta->attributes[$type] = $value;
                    continue;
                }
                $meta->attributes[$type . '_' . $key] = $value;
            }
        }
        $meta->table            = $table;
        $meta->table_id         = $table_id;
        $meta->save();
    }

    public function getMetaKeywords()
    {
        $locale = Lang::locale();
        if ($locale == 'md') $locale = 'ro';
        if ($locale != config('app.base_locale') && isset($this->meta->{'meta_description_' . $locale})) {
            return $this->meta->{'meta_description_' . $locale};
        }
        return $this->meta->{'meta_description'};
    }

    public function getMetaDescription()
    {
        $locale = Lang::locale();
        if ($locale == 'md') $locale = 'ro';
        if ($locale != config('app.base_locale') && isset($this->meta->{'meta_keywords_' . $locale})) {
            return $this->meta->{'meta_keywords_' . $locale};
        }
        return $this->meta->{'meta_keywords'};
    }

    public function mainphoto()
    {
        $fileName = "no_photo.png";
        if (isset($this->photos[0])) {
            $fileName = $this->photos[0]->source;
        }
        return $fileName;
    }

    public function mainphoto_work_plan()
    {
        $fileName = "no_photo.png";

        if (isset($this->photos_work_plan[0])) {
            $fileName = $this->photos_work_plan[0]->source;
        }
        return $fileName;
    }

    public function mainvideo()
    {
        $fileName = "";

        if (isset($this->video)) {
            $fileName = $this->video->source;
        }

        return $fileName;
    }
}
