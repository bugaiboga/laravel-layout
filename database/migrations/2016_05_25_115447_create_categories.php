<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function($t){
			$t->increments('id');
			$t->string('name');
			$t->string('name_ro');
			$t->string('name_en');
			$t->text('description');
			$t->text('description_ro');
			$t->text('description_en');
			$t->text('description_short');
			$t->text('description_short_ro');
			$t->text('description_short_en');
			$t->boolean('enabled')->default(true);
			$t->boolean('top');
            $t->integer('views');
            $t->integer('sort');
            $t->string('slug')->unique();
            $t->string('slug_ro')->unique();
            $t->string('slug_en')->unique();
            $t->string('reserve', 20);
            $t->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
