<?php

namespace App\Jobs;

use App\Models\Brand;
use App\Models\Categories;
use App\Models\Parameters;
use App\Models\ParametersValues;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Orchestra\Parser\Xml\Facade as XmlParser;
use XMLReader;

class ImportProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $filePath;
    private $textFileName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->filePath = storage_path('app/public/xml/goods.xml');
        $this->textFileName = 'goods.txt';
        app()->setLocale(config('app.base_locale'));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Parsing a large document with Expand and SimpleXML
        $xml = simplexml_load_file($this->filePath);

        $generatedDate = (string) $xml['Generated'];
        if (Storage::disk('public')->exists('xml/' . $this->textFileName)) {
            $fileContent = Storage::disk('public')->get('xml/' . $this->textFileName);
            if ($fileContent == $generatedDate) {
                echo 'Файл уже был загружен!' . PHP_EOL;
                return;
            }
        } else {
            Storage::disk('public')->put('xml/' . $this->textFileName, '');
        }

        $data = [];
        foreach ($xml->children() as $product) {
            $params = [];
            foreach ($product->product_characteristics as $param) {
                $params[] = [
                    'name' => (string) $param['product_filter_name_ru'],
                    'name_ro' => (string) $param['product_filter_name_ro'],
                    'value' => (string) $param['product_filter_value_ru'],
                    'value_ro' => (string) $param['product_filter_value_ro'],
                    'type' => (string) $param['product_filter_type'],
                ];
            }

            // for debugging !!!
            //if ((string) $product->product_vCode['product_vCode'] != 'BM.2873-05')
            //    continue;

            preg_match('/^\d+(?:[.,]\d{1,2})?$/m', $product->product_price['product_price'], $matches);
            $sale = (float) $product->product_discount['product_discount'];

            $data[] = [
                'product_code' => preg_replace( '/[^0-9]/', '', $product->product_code['product_code']),
                'product_number' => (string) $product->product_vCode['product_vCode'],
                'name' => (string) $product->product_name_ru['product_name_ru'],
                'name_ro' => (string) $product->product_name_ro['product_name_ro'],
                'description' => (string) $product->product_description_ru['product_description_ru'],
                'description_ro' => (string) $product->product_description_ro['product_description_ro'],
                'price' => (float) (isset($matches[0]) ? str_replace(",", ".", $matches[0]) : 0),
                'availability' => self::getAvailability((string) $product->product_availabile['product_availabile']),
                'brand_ru' => (string) $product->product_trademark_ru['product_trademark_ru'],
                'brand_ro' => (string) $product->product_trademark_ro['product_trademark_ro'],
                'category_ru' => (string) $product->product_category_ru['product_category_ru'],
                'category_ro' => (string) $product->product_category_ro['product_category_ro'],
                'subcategory_ru' => (string) $product->product_subcategory_ru['product_subcategory_ru'],
                'subcategory_ro' => (string) $product->product_subcategory_ro['product_subcategory_ro'],
                'sale' => (float) $product->product_discount['product_discount'],
                'recommended' => $this->getBoolean($product->product_recomended['product_recomended']),
                'top_sale' => $this->getBoolean($product->product_recomended['product_recomended']),
                'new' => $this->getBoolean($product->product_is_new['product_is_new']),
                'enabled' => $this->getBoolean($product->product_visible['product_visible']),
                'has_discount' => ($sale == 0) ? false : $this->getBoolean($product->product_has_discount['product_has_discount']),
                'params' => $params,
            ];
        }

        $productFields = [
            'product_number', 'name', 'name_ro', 'description', 'description_ro',
            'price', 'availability', 'sale', 'recommended', 'top_sale', 'new', 'enabled',
            'has_discount',
        ];

        $counter = 0;
        foreach ($data as $item) {

            $values = array_only($item, $productFields);

            // product slug
            $values['slug'] = str_slug($item['name']) . '-' . str_slug($item['product_code']);

            // ищем бренд по названию
            $values['brand_id'] = $this->getBrandId($item);

            // ищем категорию по названию
            $values['category_id'] = $this->getCategoryId($item);

            // обновляем либо создаем продукт
            $product = Product::updateOrCreate(['product_code' => $item['product_code']], $values);

            // добавляем/изменяем характеристики для товара
            $this->syncParameters($product, $item['params']);

            echo ++$counter . '. ' . $product->name . PHP_EOL;
        }


        Storage::disk('public')->put('xml/' . $this->textFileName, $generatedDate);
    }

    private function syncParameters($product, $params)
    {
        $parametersArr = Parameters::get()->pluck('id', 'name')->toArray();

        $categoryParamsIds = [];

        foreach ($params as $param) {
            if ($param['name'] == '' ||
                $param['name_ro'] == '' ||
                $param['value'] == '' ||
                $param['value_ro'] == ''
            ) continue;

            // если нет параметра, то создаем
            if (!isset($parametersArr[$param['name']])) {
                $newParam = new Parameters();
                $newParam->name = $param['name'];
                $newParam->name_ro = $param['name_ro'];
                $newParam->type = $this->getParamType($param['type']);
                if ($newParam->type == Parameters::TYPE_SELECT) {
                    $newParam->is_filter = true;
                } else {
                    $newParam->is_filter = false;
                }
                $newParam->save();
            } else {
                $paramId = $parametersArr[$param['name']];
                $newParam = Parameters::find($paramId);
            }

            // узнаем категорию товара
            $category = $product->category;

            // привязываем параметр к категории если такого не было
            if (isset($category) && ! $category->parameters->contains($newParam)) {
                $category->parameters()->save($newParam);
            }

            // для параметра типа Строка
            if ($newParam->type == Parameters::TYPE_STRING) {
                $pivot[$newParam->id] = ['value' => $param['value'], 'value_ro' => $param['value_ro']];
            }

            // для параметра типа Список
            if ($newParam->type == Parameters::TYPE_SELECT) {
                $values = $newParam->values->pluck('id', 'value')->toArray();
                if (!isset($values[$param['value']])) {
                    $parameterValue = new ParametersValues();
                    $parameterValue->value = $param['value'];
                    $parameterValue->value_ro = $param['value_ro'];
                    $parameterValue->parameter_id = $newParam->id;
                    $parameterValue->save();
                    $valueId = $parameterValue->id;
                } else {
                    $valueId = $values[$param['value']];
                }
                $pivot[$newParam->id] = ['value_id' => $valueId];
            }

            // привязываем параметр к товару
            $product->parameters()->sync($pivot);

        }
    }

    public static function getAvailability($value)
    {
        switch ($value) {
            case 'zero':
                return Product::AVAILABILITY_NOT;
            case 'enough':
                return Product::AVAILABILITY_FULL;
            case 'notenough':
                return Product::AVAILABILITY_LOW;
            case 'not_enough':
                return Product::AVAILABILITY_LOW;
        }
    }

    private function getParamType($value)
    {
        switch ($value) {
            case 'Catalog':
                return Parameters::TYPE_SELECT;
            case 'String':
                return Parameters::TYPE_STRING;
            case 'Integer':
                return Parameters::TYPE_INPUT;
        }
    }

    private function getBoolean($value)
    {
        if ((string) $value == 'true')
            return true;
        return false;
    }

    private function getBrandId($item)
    {
        // выбираем бренды
        $brandsArr = Brand::get()->pluck('id', 'name')->toArray();

        $name_ru = $item['brand_ru'];
        $name_ro = $item['brand_ro'];

        if ($name_ru == '' && $name_ro == '')
            return 0;

        // если бренд уже существует
        if (isset($brandsArr[$name_ru])) {
            return $brandsArr[$name_ru];
        }

        // если не существует
        $brand = new Brand();
        $brand->name = $name_ru;
        // $brand->name_ro = $name_ro;
        $brand->save();

        return $brand->id;
    }

    private function getCategoryId($item)
    {
        if (
            $item['category_ru'] == '' ||
            $item['category_ro'] == '' ||
            $item['subcategory_ru'] == '' ||
            $item['subcategory_ro'] == ''
        )
            return 0;

        // собираем все категории с субкатегориями в массив
        $categories = Categories::whereDoesntHave('parents')->with('children')->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $childrenArr = [];
            foreach ($category->children as $child) {
                $childrenArr[$child->name] = $child->id;
            }

            $categoriesArr[$category->name] = [
                'id' => $category->id,
                'children' => $childrenArr,
            ];
        }

        // если категория не существует, то создаем
        if (!isset($categoriesArr[$item['category_ru']])) {
            $parentCategory = new Categories();
            $parentCategory->name = $item['category_ru'];
            $parentCategory->name_ro = $item['category_ro'];
            $parentCategory->slug = str_slug($item['category_ru']);
            $parentCategory->save();

            // создаем и субкатегорию
            $child = new Categories();
            $child->name = $item['subcategory_ru'];
            $child->name_ro = $item['subcategory_ro'];
            $child->slug = str_slug($item['subcategory_ru']);
            $child->save();

            // связываем субкатегорию с родительской
            $child->parents()->sync($parentCategory->id, true);

            return $child->id;
        }

        // если субкатегория существует
        if (isset($categoriesArr[$item['category_ru']]['children'][$item['subcategory_ru']]))
            return $categoriesArr[$item['category_ru']]['children'][$item['subcategory_ru']];

        // если субкатегория не существует, то создаем
        $child = new Categories();
        $child->name = $item['subcategory_ru'];
        $child->name_ro = $item['subcategory_ro'];
        $child->slug = str_slug($item['subcategory_ru']);
        $child->save();

        // связываем субкатегорию с родительской
        $child->parents()->sync($categoriesArr[$item['category_ru']]['id'], true);

        return $child->id;
    }
}
