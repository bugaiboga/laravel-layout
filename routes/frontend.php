<?php

/*
 *
 * FRONTEND
 *
 */


Route::get('/', 'HomeController@index')->name('index');

Route::get('home', function () {
    return redirect('/');
});

Route::get('about', 'HomeController@getAbout')->name('get-about');

//Route::get('products', 'ProductController@getCategories')->name('get-categories');
//Route::get('products/{category_slug}', 'ProductController@getProductsCategory')->name('get-products-category');
Route::get('objects/{object_slug}', 'ProductController@getProduct')->name('get-product');

/*===================================
=            Mail routes            =
===================================*/

Route::post('send-contact-form', 'HomeController@sendContactForm')->name('send-contact-form');
Route::post('send-consultation-form', 'HomeController@sendConsultationForm')->name('send-consultation-form');
/*=====  End of Mail routes  ======*/

/*=====  End of Profile routes  ======*/

//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
Route::get('{slug}', ['uses'=>'ContentController@getBySlug', 'as'=>'content']);
